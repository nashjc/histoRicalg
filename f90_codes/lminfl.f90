!-----------------------------------------------------------------------
!
!   R : A Computer Language for Statistical Data Analysis
!   Copyright (C) 1996, 1997  Robert Gentleman and Ross Ihaka
!   Copyright (C) 2003-5      The R Foundation
!
!   This program is free software; you can redistribute it and/or modify
!   it under the terms of the GNU General Public License as published by
!   the Free Software Foundation; either version 2 of the License, or
!   (at your option) any later version.
!
!   This program is distributed in the hope that it will be useful,
!   but WITHOUT ANY WARRANTY; without even the implied warranty of
!   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!   GNU General Public License for more details.
!
!   You should have received a copy of the GNU General Public License
!   along with this program; if not, a copy is available at
!   https://www.R-project.org/Licenses/
!
!-----------------------------------------------------------------------
!
!   lminfl computes basic quantities useful for computing
!   regression diagnostics.
!
!   on entry
!
!       x       double precision(ldx,k)
!               the qr decomposition as computed by dqrdc or dqrdc2.
!
!       ldx     integer
!               the leading dimension of the array x.
!
!       n       integer
!               the number of rows of the matrix x.
!
!       k       integer
!               the number of columns in the matrix k.
!
!       docoef  integer (logical) indicating if  coef(*,*) should be computed
!               Computation of coef(.) is O(n^2 * k) which may be too much.
!
!       qraux   double precision(k)
!               auxiliary information about the qr decomposition.
!
!       resid   double precision(k)
!               the residuals from the regression.
!
!   on return
!
!       hat     double precision(n)
!               the diagonal of the hat matrix.
!
!       coef    double precision(n,p)
!               a matrix which has as i-th row the estimated
!               regression coefficients when the i-th case is omitted
!               from the regression.
!
!       sigma   double precision(n)
!               the i-th element of sigma contains an estimate
!               of the residual standard deviation for the model with
!               the i-th case omitted.
!
!   This version dated Aug 24, 1996.
!   Ross Ihaka, University of Auckland.
!   `docoef' option added Feb 17, 2003;  Martin Maechler ETH Zurich.
!   Handle hat == 1 case, Nov 2005.
!   Argument 'tol' was real not double precision, Aug 2007

    Subroutine lminfl(x,ldx,n,k,docoef,qraux,resid,hat,coef,sigma,tol)
!     coef(.,.) can be dummy(1) when docoef is 0(false)

!     .. Implicit None Statement ..
      Implicit None
!     .. Precision Parameter ..
      Integer, Parameter               :: wp = Kind(0.0d0)
!     .. Scalar Arguments ..
      Real (Kind=wp)                   :: tol
      Integer                          :: docoef, k, ldx, n
!     .. Array Arguments ..
      Real (Kind=wp)                   :: coef(n,k), hat(n), qraux(k),         &
                                          resid(n), sigma(n), x(ldx,k)
!     .. Local Scalars ..
      Real (Kind=wp)                   :: denom, sum
      Integer                          :: i, info, j
!     .. Local Arrays ..
      Real (Kind=wp)                   :: dummy(1)
!     .. External Procedures ..
      External                         :: dqrsl, dtrsl
!     .. Intrinsic Procedures ..
      Intrinsic                        :: sqrt
!     .. Executable Statements ..
!
!     hat matrix diagonal
!
      Do i = 1, n
        hat(i) = 0.0E0_wp
      End Do

      Do j = 1, k
        Do i = 1, n
          sigma(i) = 0.0E0_wp
        End Do
        sigma(j) = 1.0E0_wp
        Call dqrsl(x,ldx,n,k,qraux,sigma,sigma,dummy,dummy,dummy,dummy,10000,  &
          info)
        Do i = 1, n
          hat(i) = hat(i) + sigma(i)*sigma(i)
        End Do
      End Do
      Do i = 1, n
        If (hat(i)>=1.0E0_wp-tol) hat(i) = 1.0E0_wp
      End Do
!
!     changes in the estimated coefficients
!
      If (docoef/=0) Then
        Do i = 1, n
          Do j = 1, n
            sigma(j) = 0.0E0_wp
          End Do
!           if hat is effectively 1, change is zero
          If (hat(i)<1.0E0_wp) Then
            sigma(i) = resid(i)/(1.0E0_wp-hat(i))
            Call dqrsl(x,ldx,n,k,qraux,sigma,dummy,sigma,dummy,dummy,dummy,    &
              1000,info)
            Call dtrsl(x,ldx,k,sigma,1,info)
          End If
          Do j = 1, k
            coef(i,j) = sigma(j)
          End Do
        End Do
      End If
!
!     estimated residual standard deviation
!
      denom = (n-k-1)
      sum = 0.0E0_wp
      Do i = 1, n
        sum = sum + resid(i)*resid(i)
      End Do
      Do i = 1, n
        If (hat(i)<1.0E0_wp) Then
          sigma(i) = sqrt((sum-resid(i)*resid(i)/(1.0E0_wp-hat(i)))/denom)
        Else
          sigma(i) = sqrt(sum/denom)
        End If
      End Do
      Return
    End Subroutine lminfl
