    Function bvalue(t,bcoef,n,k,x,jderiv)

!     Calculates value at  x  of  jderiv-th derivative of spline from B-repr.
!     The spline is taken to be continuous from the right.
!
!     calls  interv()  (from ../../../appl/interv.c )
!
!     ******  i n p u t ******
!     t, bcoef, n, k......forms the b-representation of the spline  f  to
!        be evaluated. specifically,
!     t.....knot sequence, of length  n+k, assumed nondecreasing.
!     bcoef.....b-coefficient sequence, of length  n .
!     n.....length of  bcoef  and dimension of s(k,t),
!        a s s u m e d  positive .
!     k.....order of the spline .
!
!     w a r n i n g . . .   the restriction  k <= kmax (=20)  is imposed
!        arbitrarily by the dimension statement for  aj, dm, dm  below,
!        but is  n o w h e r e  c h e c k e d  for.
!     however in R, this is only called from bvalus() with k=4 anyway!
!
!     x.....the point at which to evaluate .
!     jderiv.....integer giving the order of the derivative to be evaluated
!        a s s u m e d  to be zero or positive.
!
!     ******  o u t p u t  ******
!     bvalue.....the value of the (jderiv)-th derivative of  f  at  x .
!
!     ******  m e t h o d  ******
!     the nontrivial knot interval  (t(i),t(i+1))  containing  x  is lo-
!     cated with the aid of  interv(). the  k  b-coeffs of  f  relevant for
!     this interval are then obtained from  bcoef (or taken to be zero if
!     not explicitly available) and are then differenced  jderiv  times to
!     obtain the b-coeffs of  (d^jderiv)f  relevant for that interval.
!     precisely, with  j = jderiv, we have from x.(12) of the text that
!
!     (d^j)f  =  sum ( bcoef(.,j)*b(.,k-j,t) )
!
!     where
!                   / bcoef(.),                     ,  j .eq. 0
!                   /
!     bcoef(.,j)  =  / bcoef(.,j-1) - bcoef(.-1,j-1)
!                   / ----------------------------- ,  j > 0
!                   /    (t(.+k-j) - t(.))/(k-j)
!
!     then, we use repeatedly the fact that
!
!     sum ( a(.)*b(.,m,t)(x) )  =  sum ( a(.,x)*b(.,m-1,t)(x) )
!     with
!                 (x - t(.))*a(.) + (t(.+m-1) - x)*a(.-1)
!     a(.,x)  =    ---------------------------------------
!                 (x - t(.))      + (t(.+m-1) - x)
!
!     to write  (d^j)f(x)  eventually as a linear combination of b-splines
!     of order  1 , and the coefficient for  b(i,1,t)(x)  must then
!     be the desired number  (d^j)f(x). (see x.(17)-(19) of text).
!
!     Arguments
!     dimension t(n+k)
!     current fortran standard makes it impossible to specify the length of
!     t  precisely without the introduction of otherwise superfluous
!     additional arguments.

!     Local Variables

!

!     initialize

!     .. Implicit None Statement ..
      Implicit None
!     .. Precision Parameter ..
      Integer, Parameter               :: wp = Kind(0.0d0)
!     .. Function Return Value ..
      Real (Kind=wp)                   :: bvalue
!     .. Parameters ..
      Integer, Parameter               :: kmax = 20
!     .. Scalar Arguments ..
      Real (Kind=wp)                   :: x
      Integer                          :: jderiv, k, n
!     .. Array Arguments ..
      Real (Kind=wp)                   :: bcoef(n), t(*)
!     .. Local Scalars ..
      Real (Kind=wp)                   :: fkmj
      Integer, Save                    :: i
      Integer                          :: ilo, imk, j, jc, jcmax, jcmin,       &
                                          jdrvp1, jj, km1, kmj, mflag, nmi
!     .. Local Arrays ..
      Real (Kind=wp)                   :: aj(kmax), dm(kmax), dp(kmax)
!     .. External Procedures ..
      Integer, External                :: interv
      External                         :: rwarn
!     .. Intrinsic Procedures ..
      Intrinsic                        :: real
!     .. Data Statements ..
      Data i/1/
!     .. Executable Statements ..

      bvalue = 0.E0_wp
      If (jderiv>=k) Go To 100
!
!     *** find  i  s.t.  1 <= i < n+k  and  t(i) < t(i+1) and
!      t(i) <= x < t(i+1) . if no such i can be found,  x  lies
!      outside the support of the spline  f and bvalue = 0.
!     {this case is handled in the calling R code}
!      (the asymmetry in this choice of  i  makes  f  rightcontinuous)
      If ((x/=t(n+1)) .Or. (t(n+1)/=t(n+k))) Then
        i = interv(t,n+k,x,0,0,i,mflag)
        If (mflag/=0) Then
          Call rwarn('bvalue()  mflag != 0: should never happen!')
          Go To 100
        End If
      Else
        i = n
      End If

!     *** if k = 1 (and jderiv = 0), bvalue = bcoef(i).
      km1 = k - 1
      If (km1<=0) Then
        bvalue = bcoef(i)
        Go To 100
      End If
!
!     *** store the k b-spline coefficients relevant for the knot interval
!     (t(i),t(i+1)) in aj(1),...,aj(k) and compute dm(j) = x - t(i+1-j),
!     dp(j) = t(i+j) - x, j=1,...,k-1 . set any of the aj not obtainable
!     from input to zero. set any t.s not obtainable equal to t(1) or
!     to t(n+k) appropriately.
      jcmin = 1
      imk = i - k
      If (imk>=0) Then
        Do j = 1, km1
          dm(j) = x - t(i+1-j)
        End Do
      Else
        jcmin = 1 - imk
        Do j = 1, i
          dm(j) = x - t(i+1-j)
        End Do
        Do j = i, km1
          aj(k-j) = 0.E0_wp
          dm(j) = dm(i)
        End Do
      End If
!
      jcmax = k
      nmi = n - i
      If (nmi>=0) Then
        Do j = 1, km1
!         the following if() happens; e.g. in   pp <- predict(cars.spl, xx)
!         -       if( (i+j) .gt. lent) write(6,9911) i+j,lent
!         -  9911         format(' i+j, lent ',2(i6,1x))
          dp(j) = t(i+j) - x
        End Do
      Else
        jcmax = k + nmi
        Do j = 1, jcmax
          dp(j) = t(i+j) - x
        End Do
        Do j = jcmax, km1
          aj(j+1) = 0.E0_wp
          dp(j) = dp(jcmax)
        End Do
      End If

!
      Do jc = jcmin, jcmax
        aj(jc) = bcoef(imk+jc)
      End Do
!
!               *** difference the coefficients  jderiv  times.
      If (jderiv>=1) Then
        Do j = 1, jderiv
          kmj = k - j
          fkmj = real(kmj,kind=wp)
          ilo = kmj
          Do jj = 1, kmj
            aj(jj) = ((aj(jj+1)-aj(jj))/(dm(ilo)+dp(jj)))*fkmj
            ilo = ilo - 1
          End Do
        End Do
      End If

!
!     *** compute value at  x  in (t(i),t(i+1)) of jderiv-th derivative,
!     given its relevant b-spline coeffs in aj(1),...,aj(k-jderiv).

      If (jderiv/=km1) Then
        jdrvp1 = jderiv + 1
        Do j = jdrvp1, km1
          kmj = k - j
          ilo = kmj
          Do jj = 1, kmj
            aj(jj) = (aj(jj+1)*dm(ilo)+aj(jj)*dp(jj))/(dm(ilo)+dp(jj))
            ilo = ilo - 1
          End Do
        End Do
      End If

      bvalue = aj(1)
!
100   Return
    End Function bvalue
