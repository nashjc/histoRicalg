!-----------------------------------------------------------------------
!
!   R : A Computer Language for Statistical Data Analysis
!   Copyright (C) 1977        B.D. Ripley
!   Copyright (C) 1999        the R Core Team
!
!   This program is free software; you can redistribute it and/or modify
!   it under the terms of the GNU General Public License as published by
!   the Free Software Foundation; either version 2 of the License, or
!   (at your option) any later version.
!
!   This program is distributed in the hope that it will be useful,
!   but WITHOUT ANY WARRANTY; without even the implied warranty of
!   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!   GNU General Public License for more details.
!
!   You should have received a copy of the GNU General Public License
!   along with this program; if not, a copy is available at
!   https://www.R-project.org/Licenses/
!
!-----------------------------------------------------------------------
!
    Subroutine eureka(lr,r,g,f,var,a)
!
!      solves Toeplitz matrix equation toep(r)f=g(1+.)
!      by Levinson's algorithm
!      a is a workspace of size lr, the number
!      of equations
!

!     .. Implicit None Statement ..
      Implicit None
!     .. Precision Parameter ..
      Integer, Parameter               :: wp = Kind(0.0d0)
!     .. Scalar Arguments ..
      Integer                          :: lr
!     .. Array Arguments ..
      Real (Kind=wp)                   :: a(lr), f(lr,lr), g(lr+1), r(lr+1),   &
                                          var(lr)
!     .. Local Scalars ..
      Real (Kind=wp)                   :: d, hold, q, v
      Integer                          :: i, j, k, l, l1, l2
!     .. Executable Statements ..
      v = r(1)
      d = r(2)
      a(1) = 1.0E0_wp
      f(1,1) = g(2)/v
      q = f(1,1)*r(2)
      var(1) = (1-f(1,1)*f(1,1))*r(1)
      If (lr==1) Return
      Do l = 2, lr
        a(l) = -d/v
        If (l>2) Then
          l1 = (l-2)/2
          l2 = l1 + 1
          Do j = 2, l2
            hold = a(j)
            k = l - j + 1
            a(j) = a(j) + a(l)*a(k)
            a(k) = a(k) + a(l)*hold
          End Do
          If (2*l1/=l-2) a(l2+1) = a(l2+1)*(1.0E0_wp+a(l))
        End If
        v = v + a(l)*d
        f(l,l) = (g(l+1)-q)/v
        Do j = 1, l - 1
          f(l,j) = f(l-1,j) + f(l,l)*a(l-j+1)
        End Do
!       estimate the innovations variance
        var(l) = var(l-1)*(1-f(l,l)*f(l,l))
        If (l==lr) Return
        d = 0.0E0_wp
        q = 0.0E0_wp
        Do i = 1, l
          k = l - i + 2
          d = d + a(i)*r(k)
          q = q + f(l,i)*r(k)
        End Do
      End Do
      Return
    End Subroutine eureka
