!   Output from Public domain Ratfor, version 1.0
    Subroutine stxwx(x,z,w,k,xknot,n,y,hs0,hs1,hs2,hs3)

!      implicit none
!     local
!

!     .. Implicit None Statement ..
      Implicit None
!     .. Precision Parameter ..
      Integer, Parameter               :: wp = Kind(0.0d0)
!     .. Scalar Arguments ..
      Integer                          :: k, n
!     .. Array Arguments ..
      Real (Kind=wp)                   :: hs0(n), hs1(n), hs2(n), hs3(n),      &
                                          w(k), x(k), xknot(n+4), y(n), z(k)
!     .. Local Scalars ..
      Real (Kind=wp)                   :: eps
      Integer                          :: i, ileft, j, lenxk, mflag
!     .. Local Arrays ..
      Real (Kind=wp)                   :: vnikx(4,1), work(16)
!     .. External Procedures ..
      Integer, External                :: interv
      External                         :: bsplvd
!     .. Executable Statements ..

      lenxk = n + 4
!     Initialise the output vectors
      Do i = 1, n
        y(i) = 0E0_wp
        hs0(i) = 0E0_wp
        hs1(i) = 0E0_wp
        hs2(i) = 0E0_wp
        hs3(i) = 0E0_wp
      End Do

!     Compute X' W^2 X -> hs0,hs1,hs2,hs3  and X' W^2 Z -> y
!     Note that here the weights w(i) == sqrt(wt[i])  where wt[] where original weights
      ileft = 1
      eps = .1E-9_wp

      Do i = 1, k
        ileft = interv(xknot(1),n+1,x(i),0,0,ileft,mflag)
!        if(mflag==-1) {write(6,'("Error in hess ",i2)')mflag;stop}
!        if(mflag==-1) {return}
        If (mflag==1) Then
          If (x(i)<=(xknot(ileft)+eps)) Then
            ileft = ileft - 1
          Else
            Return
          End If
!         else{write(6,'("Error in hess ",i2)')mflag;stop}}
        End If
        Call bsplvd(xknot,lenxk,4,x(i),ileft,work,vnikx,1)

        j = ileft - 4 + 1
        y(j) = y(j) + w(i)**2*z(i)*vnikx(1,1)
        hs0(j) = hs0(j) + w(i)**2*vnikx(1,1)**2
        hs1(j) = hs1(j) + w(i)**2*vnikx(1,1)*vnikx(2,1)
        hs2(j) = hs2(j) + w(i)**2*vnikx(1,1)*vnikx(3,1)
        hs3(j) = hs3(j) + w(i)**2*vnikx(1,1)*vnikx(4,1)
        j = ileft - 4 + 2
        y(j) = y(j) + w(i)**2*z(i)*vnikx(2,1)
        hs0(j) = hs0(j) + w(i)**2*vnikx(2,1)**2
        hs1(j) = hs1(j) + w(i)**2*vnikx(2,1)*vnikx(3,1)
        hs2(j) = hs2(j) + w(i)**2*vnikx(2,1)*vnikx(4,1)
        j = ileft - 4 + 3
        y(j) = y(j) + w(i)**2*z(i)*vnikx(3,1)
        hs0(j) = hs0(j) + w(i)**2*vnikx(3,1)**2
        hs1(j) = hs1(j) + w(i)**2*vnikx(3,1)*vnikx(4,1)
        j = ileft - 4 + 4
        y(j) = y(j) + w(i)**2*z(i)*vnikx(4,1)
        hs0(j) = hs0(j) + w(i)**2*vnikx(4,1)**2

      End Do

      Return
    End Subroutine stxwx
