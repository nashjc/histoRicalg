---
output:
  pdf_document: default
title: "Algorithm 9: Inverse of a positive definite symmetric matrix in situ"
author: "John C Nash,
    Telfer School of Management,
    University of Ottawa,
    nashjc@uottawa.ca"
date: Sept 22, 2017
bibliography: ../historicalg.bib
---

# Background

Bauer and Reinsch presented a very compact algorithm for inverting a positive-definite
symmetric matrix *in situ*, that is, overwriting the original matrix. 


# Implementation


# Format for the templates

Documentation of the method

# References

