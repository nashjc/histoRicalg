This is a document listing the active items for the histoRicalg
project.

We see histoRicalg as a volunteer project which accomodates those
interested in older algorithms, numerical analysis, and computation
generally. We are mostly focused on **R** as a high-level data-first
statistical programming language of choice for convenient algorithm
description and data handling; however, the nature of this effort has
so far required advanced approaches for the reading, interpretation
and even compiling and running C, Fortran and Algol from a variety of
standards, mainly vintage.

Since we do not know what you are interested or capable of providing,
individuals are welcome to review the
[./histoRicalg/background](https://gitlab.com/nashjc/histoRicalg/tree/master/background)
materials which although largely administrative and not reflective of
most of the progress we have made, will supply unfamiliar readers with
context about the origins and aims of this project. For further detail
please refer to the
[wiki](https://gitlab.com/nashjc/histoRicalg/wikis/home).

More specific project details are contained here below.

[./histoRicalg/baseR-codes](https://gitlab.com/nashjc/histoRicalg/tree/master/baseR-codes)
- This is a collection of the source code unerpinning **R**. All of
the Fortran routines are included since there aren't that many. Only C
codes of relevance are being added incrementally since there are many
of those. We are incrementally studying these algorithms, researching
their provenance and testing them. The issue being addressed is lack
of: transparent code documentation and availability of test results.

Help is needed:

- to index the codes with short descriptive notes allowing histoRicalg
members to prioritize which codes we need to document and test, both
Fortran and C. Some codes we may say are "utilities" which are less
relevant to the histoRicalg project, e.g., they may simply allow for
"printing" from Fortran or some similar capability.


[./histoRicalg/baseR-nlm-issues](https://gitlab.com/nashjc/histoRicalg/tree/master/baseR-nlm-issues/)
- This is a special folder related to a bug in the **R** codes for
`nlm` found originally in 2017 by JC Nash. At the time of this writing
(2018-10-14) we believe issues have been fixed and patched in
**R**. The code is long and complicated, and has NOT been
well-documented, but that task is, we believe, a quite large one, of
the level of a term project or even a master's level thesis if
subtests and tests are added.

[./histoRicalg/baseR-vignettes_wip](https://gitlab.com/nashjc/histoRicalg/tree/master/baseR-vignettes_wip)
- This is a collection of directories for the ongoing documentation
efforts we have begun on the provenance of the base-R methods. These
documents resemble vignettes since they incorporate and detail a
siginficant amount of R-code testing yet the subjects they cover may
be academic and sourced from all manner of journals and texts.

[./histoRicalg/calgo-nummeths](https://gitlab.com/nashjc/histoRicalg/tree/master/calgo-nummeths)
- This is a tangential project looking at documenting and translating
older codes from numerical algorithms archives such as the Collected
Algorithms (CALGO) of the ACM into **R**.  Other archives have been
identified; however, we see the CALGO as distinct in its presentation
of the codes *including* close links to key archival material,
specifically the original research and ACM Certifications. This makes
CALGO a unique jumping off point into the study of some really seminal
work between 1960-2018.

- Each entry by its self may not be particularly exciting or relevant to
modern algorithms researchers; however, there are many of these and it
has been an interesting exercise to consider the history various
routines and the technologies and approaches used. While we have found
the associated codes to each individual research project may be
tangled, obsolete, undocumented and long, there are many exceptions to
this.

- We have only just begun starting on the earliest algorithms and
although there is nothing new here for older workers this activity has
been most rewarding and an interesting side-project for beginners new
to the study of algorithms. A growing index is presented here
[./calgo-nummeths/_calgo_histoRicalg_index.pdf](https://gitlab.com/nashjc/histoRicalg/blob/master/calgo-nummeths/_calgo_histoRicalg_index.pdf).

- While there is much potential for studying more
contemporary work from this archive. We have found that the older
texts often present what is currently advanced, complex or assumed
knowledge in a sometimes simpler or more direct manner. Prospective
contributors are welcome to start with whatever interests them.

- Note that some early codes may, as with the CORDIC codes
(https://en.wikipedia.org/wiki/CORDIC) be useful for new computing
architectures such as quantum computers. Thanks to Peter Selinger for
pointing us to this.

[./histoRicalg/media](https://gitlab.com/nashjc/histoRicalg/tree/master/media)
- Here we have collected some materials for our public presentation of
the project including slides and demonstration documents.

[./histoRicalg/misc-vignettes](https://gitlab.com/nashjc/histoRicalg/tree/master/misc-vignettes)
- Here we have a cheat sheet for the computing of otherwise inoperable
codes out of date with respect to modern compilers. This includes
information on `knitr` (which is a modern document compiler), `Fortran
77`, `Algol 60`, and `BASIC`. Keeping these codes running is of
particular interest to the project originator. Particular tasks
related to this aspect of the project are:

- Establishing working compilers on different platforms for different
  "older" computing languages. So far Linux is quite well-served, but
  documentation for Windows would be helpful. Note that we have not
  got good tests or validated language processors for Algol, AlgolW,
  or PL/I. Each such language and test is a relatively small task,
  though the ensemble is quite a challenge.

- Comparison of some of the older codes with their supposed derived
  codes, for example, in providing solutions of linear equations,
  eigenvalues or similar tasks. Note that the early codes predated the
  standardization of computer arithmetic (i.e., the 1985 IEEE-754
  standard).


Questions on this material or anything else are best directed to the
mailing list which is linked to in the
[README](https://gitlab.com/nashjc/histoRicalg/blob/master/README.md).

Thank you for your attention.

