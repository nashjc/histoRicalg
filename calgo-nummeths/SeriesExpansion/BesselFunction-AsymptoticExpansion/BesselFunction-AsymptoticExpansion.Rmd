---
title: "Bessel Function - Asymptotic Expansion"
author: 
date: "`r format(Sys.time(), '%B %d, %Y')`"
output: 
   pdf_document:
     citation_package: "natbib"
bibliography: BesselFunction-AsymptoticExpansion.bib
---
\citep{Clarke:1960:ABF:367177.377894}
```{r include=TRUE}
sessionInfo()
```

```{r setup, include=TRUE}

```