/* Jose R. Valverde. 2 February 1995 */
/*
Computes a value of g=x satisfying the equation x=f(x)
*/

double Root(f, a, epsilon)
double (*f)(double);
double a;
double epsilon;
{
    /* Root */
    double b, c, d, e,g;

    b = a;
    c = (*f)(b);
    g = c;
    if (c == a)
    	return (g);
    d = a;
    b = c;
    e = c;
    for (;;) {		/* Hob: */
	c = (*f)(b);
	g = (d * c - b * e) / (c - e - b + d);
	if (fabs((g - b)/g) <= epsilon)
	    return (g);
	e = c;
	d = b;
	b = g;
    }
}
