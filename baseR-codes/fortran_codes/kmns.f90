!   Code in this file based on Applied Statistics algorithms
!   (C) Royal Statistical Society 1979
!
!   a minimal modification of AS136 to use double precision
!   all variables are now declared.
!   B.D. Ripley 1998/06/17
!   Declaration re-ordering to satisfy "f77 -ansi",  M.Maechler 2001/04/12
!
!   ~= R's  kmeans(x=A, centers=C, iter.max=ITER, algorithm = "Hartigan-Wong")
!
    Subroutine kmns(a,m,n,c,k,ic1,ic2,nc,an1,an2,ncp,d,itran,live,iter,wss,    &
      ifault)
!
!     ALGORITHM AS 136  APPL. STATIST. (1979) VOL.28, NO.1
!
!     Divide M points in N-dimensional space into K clusters so that
!     the within cluster sum of squares is minimized.
!
!                      ------        ------
!                     data x[,]      centers[,]
!
!     Define BIG to be a very large positive number
!

!     .. Implicit None Statement ..
      Implicit None
!     .. Precision Parameter ..
      Integer, Parameter               :: wp = Kind(0.0d0)
!     .. Scalar Arguments ..
      Integer                          :: ifault, iter, k, m, n
!     .. Array Arguments ..
      Real (Kind=wp)                   :: a(m,n), an1(k), an2(k), c(k,n),      &
                                          d(m), wss(k)
      Integer                          :: ic1(m), ic2(m), itran(k), live(k),   &
                                          nc(k), ncp(k)
!     .. Local Scalars ..
      Real (Kind=wp)                   :: aa, da, db, dc, temp
      Real (Kind=wp), Save             :: big, one, zero
      Integer                          :: i, ii, ij, il, imaxqtr, indx,        &
                                          itrace, j, l
!     .. Local Arrays ..
      Real (Kind=wp)                   :: dt(2)
!     .. External Procedures ..
      External                         :: kmns1, optra, qtran
!     .. Intrinsic Procedures ..
      Intrinsic                        :: real
!     .. Data Statements ..
      Data big/1.E30_wp/, zero/0.0_wp/, one/1.0_wp/
!     .. Executable Statements ..
!
      itrace = ifault
      imaxqtr = itran(1)
      ifault = 3
      If (k<=1 .Or. k>=m) Return
      ifault = 0
!
!     For each point I, find its two closest centres, IC1(I) and
!     IC2(I).     Assign it to IC1(I).
!
      Do i = 1, m
        ic1(i) = 1
        ic2(i) = 2
        Do il = 1, 2
          dt(il) = zero
          Do j = 1, n
            da = a(i,j) - c(il,j)
            dt(il) = dt(il) + da*da
          End Do
        End Do ! IL
        If (dt(1)>dt(2)) Then
          ic1(i) = 2
          ic2(i) = 1
          temp = dt(1)
          dt(1) = dt(2)
          dt(2) = temp
        End If
        Do l = 3, k
          db = zero
          Do j = 1, n
            dc = a(i,j) - c(l,j)
            db = db + dc*dc
            If (db>=dt(2)) Go To 100
          End Do
          If (db>=dt(1)) Then
            dt(2) = db
            ic2(i) = l
          Else
            dt(2) = dt(1)
            ic2(i) = ic1(i)
            dt(1) = db
            ic1(i) = l
          End If
100     End Do
      End Do
!
!     Update cluster centres to be the average of points contained
!     within them.
!     NC(L) := #{units in cluster L},  L = 1..K
      Do l = 1, k
        nc(l) = 0
        Do j = 1, n
          c(l,j) = zero
        End Do
      End Do
      Do i = 1, m
        l = ic1(i)
        nc(l) = nc(l) + 1
        Do j = 1, n
          c(l,j) = c(l,j) + a(i,j)
        End Do
      End Do
!
!     Check to see if there is any empty cluster at this stage
!
      Do l = 1, k
        If (nc(l)==0) Then
          ifault = 1
          Return
        End If
        aa = nc(l)
        Do j = 1, n
          c(l,j) = c(l,j)/aa
        End Do
!
!       Initialize AN1, AN2, ITRAN & NCP
!       AN1(L) = NC(L) / (NC(L) - 1)
!       AN2(L) = NC(L) / (NC(L) + 1)
!       ITRAN(L) = 1 if cluster L is updated in the quick-transfer stage,
!              = 0 otherwise
!       In the optimal-transfer stage, NCP(L) stores the step at which
!       cluster L is last updated.
!       In the quick-transfer stage, NCP(L) stores the step at which
!       cluster L is last updated plus M.
!
        an2(l) = aa/(aa+one)
        an1(l) = big
        If (aa>one) an1(l) = aa/(aa-one)
        itran(l) = 1
        ncp(l) = -1
      End Do

      indx = 0
      Do ij = 1, iter
!
!       OPtimal-TRAnsfer stage: there is only one pass through the data.
!       Each point is re-allocated, if necessary, to the cluster that will
!       induce the maximum reduction in within-cluster sum of squares.
!
        Call optra(a,m,n,c,k,ic1,ic2,nc,an1,an2,ncp,d,itran,live,indx)

        If (itrace>0) Call kmns1(k,ij,indx)
!
!       Stop if no transfer took place in the last M optimal transfer steps.
        If (indx==m) Go To 110

!
!       Quick-TRANSfer stage: Each point is tested in turn to see if it should
!       be re-allocated to the cluster to which it is most likely to be
!       transferred, IC2(I), from its present cluster, IC1(I).
!       Loop through the data until no further change is to take place.
!
        Call qtran(a,m,n,c,k,ic1,ic2,nc,an1,an2,ncp,d,itran,indx,itrace,       &
          imaxqtr)
!
        If (imaxqtr<0) Then
          ifault = 4
          Go To 110
        End If
!
!       If there are only two clusters, there is no need to re-enter the
!       optimal transfer stage.
!
        If (k==2) Go To 110
!
!       NCP has to be set to 0 before entering OPTRA.
!
        Do l = 1, k
          ncp(l) = 0
        End Do

      End Do ! iter --------------------------------------
!
!     Since the specified number of iterations has been exceeded, set
!     IFAULT = 2.   This may indicate unforeseen looping.
!
      ifault = 2

110   Continue
      iter = ij
!
!     Compute within-cluster sum of squares for each cluster.
!
      Do l = 1, k
        wss(l) = zero
        Do j = 1, n
          c(l,j) = zero
        End Do
      End Do

      Do i = 1, m
        ii = ic1(i)
        Do j = 1, n
          c(ii,j) = c(ii,j) + a(i,j)
        End Do
      End Do

      Do j = 1, n
        Do l = 1, k
          c(l,j) = c(l,j)/real(nc(l),kind=wp)
        End Do
        Do i = 1, m
          ii = ic1(i)
          da = a(i,j) - c(ii,j)
          wss(ii) = wss(ii) + da*da
        End Do
      End Do
!
      Return
    End Subroutine kmns
!
!
    Subroutine optra(a,m,n,c,k,ic1,ic2,nc,an1,an2,ncp,d,itran,live,indx)
!
!     ALGORITHM AS 136.1  APPL. STATIST. (1979) VOL.28, NO.1
!
!     This is the OPtimal TRAnsfer stage.
!                 ----------------------
!     Each point is re-allocated, if necessary, to the cluster that
!     will induce a maximum reduction in the within-cluster sum of
!     squares.
!

!
!     BIG := a very large positive number

!     .. Implicit None Statement ..
      Implicit None
!     .. Precision Parameter ..
      Integer, Parameter               :: wp = Kind(0.0d0)
!     .. Scalar Arguments ..
      Integer                          :: indx, k, m, n
!     .. Array Arguments ..
      Real (Kind=wp)                   :: a(m,n), an1(k), an2(k), c(k,n), d(m)
      Integer                          :: ic1(m), ic2(m), itran(k), live(k),   &
                                          nc(k), ncp(k)
!     .. Local Scalars ..
      Real (Kind=wp)                   :: al1, al2, alt, alw, da, db, dc, dd,  &
                                          de, df, r2, rr
      Real (Kind=wp), Save             :: big, one, zero
      Integer                          :: i, j, l, l1, l2, ll
!     .. Data Statements ..
      Data big/1.0E30_wp/, zero/0.0_wp/, one/1.0_wp/
!     .. Executable Statements ..
!
!     If cluster L is updated in the last quick-transfer stage, it
!     belongs to the live set throughout this stage.   Otherwise, at
!     each step, it is not in the live set if it has not been updated
!     in the last M optimal transfer steps.
!
      Do l = 1, k
        If (itran(l)==1) live(l) = m + 1
      End Do

!     ---------------------- Loop over each point -------------------
      Do i = 1, m
        indx = indx + 1
        l1 = ic1(i)
        l2 = ic2(i)
        ll = l2
!
!       If point I is the only member of cluster L1, no transfer.
!
        If (nc(l1)==1) Go To 110
!
!       If L1 has not yet been updated in this stage, no need to
!       re-compute D(I).
!
        If (ncp(l1)/=0) Then
          de = zero
          Do j = 1, n
            df = a(i,j) - c(l1,j)
            de = de + df*df
          End Do
          d(i) = de*an1(l1)
        End If
!
!       Find the cluster with minimum R2.
!
        da = zero
        Do j = 1, n
          db = a(i,j) - c(l2,j)
          da = da + db*db
        End Do
        r2 = da*an2(l2)
        Do l = 1, k
!
!         If I >= LIVE(L1), then L1 is not in the live set.   If this is
!         true, we only need to consider clusters that are in the live set
!         for possible transfer of point I.   Otherwise, we need to consider
!         all possible clusters.
!
          If (i>=live(l1) .And. i>=live(l) .Or. l==l1 .Or. l==ll) Go To 100
          rr = r2/an2(l)
          dc = zero
          Do j = 1, n
            dd = a(i,j) - c(l,j)
            dc = dc + dd*dd
            If (dc>=rr) Go To 100
          End Do
          r2 = dc*an2(l)
          l2 = l
100     End Do
        If (r2>=d(i)) Then
!         If no transfer is necessary, L2 is the new IC2(I).
          ic2(i) = l2
        Else
!
!         Update cluster centres, LIVE, NCP, AN1 & AN2 for clusters L1 and
!         L2, and update IC1(I) & IC2(I).
          indx = 0
          live(l1) = m + i
          live(l2) = m + i
          ncp(l1) = i
          ncp(l2) = i
          al1 = nc(l1)
          alw = al1 - one
          al2 = nc(l2)
          alt = al2 + one
          Do j = 1, n
            c(l1,j) = (c(l1,j)*al1-a(i,j))/alw
            c(l2,j) = (c(l2,j)*al2+a(i,j))/alt
          End Do
          nc(l1) = nc(l1) - 1
          nc(l2) = nc(l2) + 1
          an2(l1) = alw/al1
          an1(l1) = big
          If (alw>one) an1(l1) = alw/(alw-one)
          an1(l2) = alt/al2
          an2(l2) = alt/(alt+one)
          ic1(i) = l2
          ic2(i) = l1
        End If

110     Continue
        If (indx==m) Return

      End Do
!     ---------------------- each point -------------------

      Do l = 1, k
        itran(l) = 0 ! before entering QTRAN.
!       LIVE(L) has to be decreased by M before re-entering OPTRA
        live(l) = live(l) - m
      End Do
!
      Return
    End Subroutine optra
!
!
    Subroutine qtran(a,m,n,c,k,ic1,ic2,nc,an1,an2,ncp,d,itran,indx,itrace,     &
      imaxqtr)
!
!     ALGORITHM AS 136.2  APPL. STATIST. (1979) VOL.28, NO.1
!
!     This is the Quick TRANsfer stage.
!                 --------------------
!     IC1(I) is the cluster which point I belongs to.
!     IC2(I) is the cluster which point I is most likely to be
!         transferred to.
!     For each point I, IC1(I) & IC2(I) are switched, if necessary, to
!     reduce within-cluster sum of squares.  The cluster centres are
!     updated after each step.
!

!
!     Define BIG to be a very large positive number
!

!     .. Implicit None Statement ..
      Implicit None
!     .. Precision Parameter ..
      Integer, Parameter               :: wp = Kind(0.0d0)
!     .. Scalar Arguments ..
      Integer                          :: imaxqtr, indx, itrace, k, m, n
!     .. Array Arguments ..
      Real (Kind=wp)                   :: a(m,n), an1(k), an2(k), c(k,n), d(m)
      Integer                          :: ic1(m), ic2(m), itran(k), nc(k),     &
                                          ncp(k)
!     .. Local Scalars ..
      Real (Kind=wp)                   :: al1, al2, alt, alw, da, db, dd, de,  &
                                          r2
      Real (Kind=wp), Save             :: big, one, zero
      Integer                          :: i, icoun, istep, j, l1, l2
!     .. External Procedures ..
      External                         :: kmnsqpr, rchkusr
!     .. Data Statements ..
      Data big/1.0E30_wp/, zero/0.0_wp/, one/1.0_wp/
!     .. Executable Statements ..
!
!     In the quick transfer stage, NCP(L)
!     is equal to the step at which cluster L is last updated plus M.
!
      icoun = 0
      istep = 0
!     Repeat {
100   Continue

      Do i = 1, m
!       only from second "round" on
        If (itrace>0 .And. istep>=1 .And. i==1) Call kmnsqpr(istep,icoun,ncp,  &
          k,itrace)
        icoun = icoun + 1
        istep = istep + 1
        If (istep>=imaxqtr) Then
          imaxqtr = -1
          Return
        End If
        l1 = ic1(i)
        l2 = ic2(i)
!
!       If point I is the only member of cluster L1, no transfer.
!
        If (nc(l1)==1) Go To 110
!
!       If ISTEP > NCP(L1), no need to re-compute distance from point I to
!       cluster L1.   Note that if cluster L1 is last updated exactly M
!       steps ago, we still need to compute the distance from point I to
!       cluster L1.
!
        If (istep<=ncp(l1)) Then
          da = zero
          Do j = 1, n
            db = a(i,j) - c(l1,j)
            da = da + db*db
          End Do
          d(i) = da*an1(l1)
        End If
!
!       If ISTEP >= both NCP(L1) & NCP(L2) there will be no transfer of
!       point I at this step.
!
        If (istep<ncp(l1) .Or. istep<ncp(l2)) Then
          r2 = d(i)/an2(l2)
          dd = zero
          Do j = 1, n
            de = a(i,j) - c(l2,j)
            dd = dd + de*de
            If (dd>=r2) Go To 110
          End Do
!
!         Update cluster centres, NCP, NC, ITRAN, AN1 & AN2 for clusters
!         L1 & L2.   Also update IC1(I) & IC2(I).   Note that if any
!         updating occurs in this stage, INDX is set back to 0.
!
          icoun = 0
          indx = 0
          itran(l1) = 1
          itran(l2) = 1
          ncp(l1) = istep + m
          ncp(l2) = istep + m
          al1 = nc(l1)
          alw = al1 - one
          al2 = nc(l2)
          alt = al2 + one
          Do j = 1, n
            c(l1,j) = (c(l1,j)*al1-a(i,j))/alw
            c(l2,j) = (c(l2,j)*al2+a(i,j))/alt
          End Do
          nc(l1) = nc(l1) - 1
          nc(l2) = nc(l2) + 1
          an2(l1) = alw/al1
          an1(l1) = big
          If (alw>one) an1(l1) = alw/(alw-one)
          an1(l2) = alt/al2
          an2(l2) = alt/(alt+one)
          ic1(i) = l2
          ic2(i) = l1
        End If
!
!       If no re-allocation took place in the last M steps, return.
!
110     If (icoun==m) Return
      End Do

      Call rchkusr ! allow user interrupt
      Go To 100
!     --------
    End Subroutine qtran
