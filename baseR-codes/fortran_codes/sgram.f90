!   Output from Public domain Ratfor, version 1.0

!   PURPOSE
!       Calculation of the cubic B-spline smoothness prior
!       for "usual" interior knot setup.
!       Uses BSPVD and INTRV in the CMLIB
!       sgm[0-3](nb)    Symmetric matrix 'SIGMA'
!                       whose (i,j)'th element contains the integral of
!                       B''(i,.) B''(j,.) , i=1,2 ... nb and j=i,...nb.
!                       Only the upper four diagonals are computed.

    Subroutine sgram(sg0,sg1,sg2,sg3,tb,nb)

!      implicit none
!     indices
!     -------------
!

!     .. Implicit None Statement ..
      Implicit None
!     .. Precision Parameter ..
      Integer, Parameter               :: wp = Kind(0.0d0)
!     .. Scalar Arguments ..
      Integer                          :: nb
!     .. Array Arguments ..
      Real (Kind=wp)                   :: sg0(nb), sg1(nb), sg2(nb), sg3(nb),  &
                                          tb(nb+4)
!     .. Local Scalars ..
      Real (Kind=wp)                   :: wpt
      Integer                          :: i, ii, ileft, jj, lentb, mflag
!     .. Local Arrays ..
      Real (Kind=wp)                   :: vnikx(4,3), work(16), yw1(4), yw2(4)
!     .. External Procedures ..
      Integer, External                :: interv
      External                         :: bsplvd
!     .. Executable Statements ..

      lentb = nb + 4
!     Initialise the sigma vectors
      Do i = 1, nb
        sg0(i) = 0.E0_wp
        sg1(i) = 0.E0_wp
        sg2(i) = 0.E0_wp
        sg3(i) = 0.E0_wp
      End Do

      ileft = 1
      Do i = 1, nb
!        Calculate a linear approximation to the second derivative of the
!        non-zero B-splines over the interval [tb(i),tb(i+1)].
        ileft = interv(tb(1),nb+1,tb(i),0,0,ileft,mflag)

!        Left end second derivatives
        Call bsplvd(tb,lentb,4,tb(i),ileft,work,vnikx,3)

!        Put values into yw1
        Do ii = 1, 4
          yw1(ii) = vnikx(ii,3)
        End Do

!        Right end second derivatives
        Call bsplvd(tb,lentb,4,tb(i+1),ileft,work,vnikx,3)

!        Slope*(length of interval) in Linear Approximation to B''
        Do ii = 1, 4
          yw2(ii) = vnikx(ii,3) - yw1(ii)
        End Do

!        Calculate Contributions to the sigma vectors
        wpt = tb(i+1) - tb(i)
        If (ileft>=4) Then
          Do ii = 1, 4
            jj = ii
            sg0(ileft-4+ii) = sg0(ileft-4+ii) + wpt*(yw1(ii)*yw1(jj)+(yw2(ii)* &
              yw1(jj)+yw2(jj)*yw1(ii))*0.5E0_wp+yw2(ii)*yw2(jj)*0.3330E0_wp)
            jj = ii + 1
            If (jj<=4) Then
              sg1(ileft+ii-4) = sg1(ileft+ii-4) + wpt*(yw1(ii)*yw1(jj)+(yw2(ii &
                )*yw1(jj)+yw2(jj)*yw1(ii))*0.5E0_wp+yw2(ii)*yw2(jj)*           &
                0.3330E0_wp)
            End If
            jj = ii + 2
            If (jj<=4) Then
              sg2(ileft+ii-4) = sg2(ileft+ii-4) + wpt*(yw1(ii)*yw1(jj)+(yw2(ii &
                )*yw1(jj)+yw2(jj)*yw1(ii))*0.5E0_wp+yw2(ii)*yw2(jj)*           &
                0.3330E0_wp)
            End If
            jj = ii + 3
            If (jj<=4) Then
              sg3(ileft+ii-4) = sg3(ileft+ii-4) + wpt*(yw1(ii)*yw1(jj)+(yw2(ii &
                )*yw1(jj)+yw2(jj)*yw1(ii))*0.5E0_wp+yw2(ii)*yw2(jj)*           &
                0.3330E0_wp)
            End If
          End Do

        Else If (ileft==3) Then
          Do ii = 1, 3
            jj = ii
            sg0(ileft-3+ii) = sg0(ileft-3+ii) + wpt*(yw1(ii)*yw1(jj)+(yw2(ii)* &
              yw1(jj)+yw2(jj)*yw1(ii))*0.5E0_wp+yw2(ii)*yw2(jj)*0.3330E0_wp)
            jj = ii + 1
            If (jj<=3) Then
              sg1(ileft+ii-3) = sg1(ileft+ii-3) + wpt*(yw1(ii)*yw1(jj)+(yw2(ii &
                )*yw1(jj)+yw2(jj)*yw1(ii))*0.5E0_wp+yw2(ii)*yw2(jj)*           &
                0.3330E0_wp)
            End If
            jj = ii + 2
            If (jj<=3) Then
              sg2(ileft+ii-3) = sg2(ileft+ii-3) + wpt*(yw1(ii)*yw1(jj)+(yw2(ii &
                )*yw1(jj)+yw2(jj)*yw1(ii))*0.5E0_wp+yw2(ii)*yw2(jj)*           &
                0.3330E0_wp)
            End If
          End Do

        Else If (ileft==2) Then
          Do ii = 1, 2
            jj = ii
            sg0(ileft-2+ii) = sg0(ileft-2+ii) + wpt*(yw1(ii)*yw1(jj)+(yw2(ii)* &
              yw1(jj)+yw2(jj)*yw1(ii))*0.5E0_wp+yw2(ii)*yw2(jj)*0.3330E0_wp)
            jj = ii + 1
            If (jj<=2) Then
              sg1(ileft+ii-2) = sg1(ileft+ii-2) + wpt*(yw1(ii)*yw1(jj)+(yw2(ii &
                )*yw1(jj)+yw2(jj)*yw1(ii))*0.5E0_wp+yw2(ii)*yw2(jj)*           &
                0.3330E0_wp)
            End If
          End Do


        Else If (ileft==1) Then
          Do ii = 1, 1
            jj = ii
            sg0(ileft-1+ii) = sg0(ileft-1+ii) + wpt*(yw1(ii)*yw1(jj)+(yw2(ii)* &
              yw1(jj)+yw2(jj)*yw1(ii))*0.5E0_wp+yw2(ii)*yw2(jj)*0.3330E0_wp)
          End Do
        End If

      End Do

      Return
    End Subroutine sgram
