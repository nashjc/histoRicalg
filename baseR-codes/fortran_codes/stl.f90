!
!     from netlib/a/stl: no authorship nor copyright claim in the source;
!     presumably by the authors of
!
!     R.B. Cleveland, W.S.Cleveland, J.E. McRae, and I. Terpenning,
!     STL: A Seasonal-Trend Decomposition Procedure Based on Loess,
!     Statistics Research Report, AT&T Bell Laboratories.
!
!     Converted to double precision by B.D. Ripley 1999.
!     Indented, goto labels renamed, many goto's replaced by `if then {else}'
!     (using Emacs), many more comments;  by M.Maechler 2001-02.
!
    Subroutine stl(y,n,np,ns,nt,nl,isdeg,itdeg,ildeg,nsjump,ntjump,nljump,ni,  &
      no,rw,season,trend,work)

!     implicit none
!     Arg
!       n                   : length(y)
!       ns, nt, nl          : spans        for `s', `t' and `l' smoother
!       isdeg, itdeg, ildeg : local degree for `s', `t' and `l' smoother
!       nsjump,ntjump,nljump: ........     for `s', `t' and `l' smoother
!       ni, no              : number of inner and outer (robust) iterations

!     Var

!     .. Implicit None Statement ..
      Implicit None
!     .. Precision Parameter ..
      Integer, Parameter               :: wp = Kind(0.0d0)
!     .. Scalar Arguments ..
      Integer                          :: ildeg, isdeg, itdeg, n, ni, nl,      &
                                          nljump, no, np, ns, nsjump, nt,      &
                                          ntjump
!     .. Array Arguments ..
      Real (Kind=wp)                   :: rw(n), season(n), trend(n),          &
                                          work(n+2*np,5), y(n)
!     .. Local Scalars ..
      Integer                          :: i, k, newnl, newnp, newns, newnt
      Logical                          :: userw
!     .. External Procedures ..
      External                         :: stlrwt, stlstp
!     .. Intrinsic Procedures ..
      Intrinsic                        :: max, mod
!     .. Executable Statements ..

      userw = .False.
      Do i = 1, n
        trend(i) = 0.E0_wp
      End Do
!     the three spans must be at least three and odd:
      newns = max(3,ns)
      newnt = max(3,nt)
      newnl = max(3,nl)
      If (mod(newns,2)==0) newns = newns + 1
      If (mod(newnt,2)==0) newnt = newnt + 1
      If (mod(newnl,2)==0) newnl = newnl + 1
!     periodicity at least 2:
      newnp = max(2,np)

      k = 0
!     --- outer loop -- robustnes iterations
100   Continue
      Call stlstp(y,n,newnp,newns,newnt,newnl,isdeg,itdeg,ildeg,nsjump,ntjump, &
        nljump,ni,userw,rw,season,trend,work)
      k = k + 1
      If (k>no) Go To 110

      Do i = 1, n
        work(i,1) = trend(i) + season(i)
      End Do
      Call stlrwt(y,n,work(1,1),rw)
      userw = .True.
      Go To 100
!     --- end Loop
110   Continue

!     robustness weights when there were no robustness iterations:
      If (no<=0) Then
        Do i = 1, n
          rw(i) = 1.E0_wp
        End Do
      End If
      Return
    End Subroutine stl

    Subroutine stless(y,n,len,ideg,njump,userw,rw,ys,res)

!     implicit none
!     Arg
!     Var

!     .. Implicit None Statement ..
      Implicit None
!     .. Precision Parameter ..
      Integer, Parameter               :: wp = Kind(0.0d0)
!     .. Scalar Arguments ..
      Integer                          :: ideg, len, n, njump
      Logical                          :: userw
!     .. Array Arguments ..
      Real (Kind=wp)                   :: res(n), rw(n), y(n), ys(n)
!     .. Local Scalars ..
      Real (Kind=wp)                   :: delta
      Integer                          :: i, j, k, newnj, nleft, nright, nsh
      Logical                          :: ok
!     .. External Procedures ..
      External                         :: stlest
!     .. Intrinsic Procedures ..
      Intrinsic                        :: min, real
!     .. Executable Statements ..

      If (n<2) Then
        ys(1) = y(1)
        Return
      End If

      newnj = min(njump,n-1)
      If (len>=n) Then
        nleft = 1
        nright = n
        Do i = 1, n, newnj
          Call stlest(y,n,len,ideg,real(i,kind=wp),ys(i),nleft,nright,res,     &
            userw,rw,ok)
          If (.Not. ok) ys(i) = y(i)
        End Do

      Else

        If (newnj==1) Then
          nsh = (len+1)/2
          nleft = 1
          nright = len
          Do i = 1, n
            If (i>nsh .And. nright/=n) Then
              nleft = nleft + 1
              nright = nright + 1
            End If
            Call stlest(y,n,len,ideg,real(i,kind=wp),ys(i),nleft,nright,res,   &
              userw,rw,ok)
            If (.Not. ok) ys(i) = y(i)
          End Do
        Else
          nsh = (len+1)/2
          Do i = 1, n, newnj
            If (i<nsh) Then
              nleft = 1
              nright = len
            Else If (i>=n-nsh+1) Then
              nleft = n - len + 1
              nright = n
            Else
              nleft = i - nsh + 1
              nright = len + i - nsh
            End If

            Call stlest(y,n,len,ideg,real(i,kind=wp),ys(i),nleft,nright,res,   &
              userw,rw,ok)
            If (.Not. ok) ys(i) = y(i)
          End Do

        End If

      End If

      If (newnj/=1) Then
        Do i = 1, n - newnj, newnj
          delta = (ys(i+newnj)-ys(i))/real(newnj,kind=wp)
          Do j = i + 1, i + newnj - 1
            ys(j) = ys(i) + delta*real(j-i,kind=wp)
          End Do
        End Do
        k = ((n-1)/newnj)*newnj + 1

        If (k/=n) Then
          Call stlest(y,n,len,ideg,real(n,kind=wp),ys(n),nleft,nright,res,     &
            userw,rw,ok)
          If (.Not. ok) ys(n) = y(n)

          If (k/=n-1) Then
            delta = (ys(n)-ys(k))/real(n-k,kind=wp)
            Do j = k + 1, n - 1
              ys(j) = ys(k) + delta*real(j-k,kind=wp)
            End Do
          End If
        End If
      End If
      Return
    End Subroutine stless

    Subroutine stlest(y,n,len,ideg,xs,ys,nleft,nright,w,userw,rw,ok)

!     implicit none
!     Arg
!     Var

!     .. Implicit None Statement ..
      Implicit None
!     .. Precision Parameter ..
      Integer, Parameter               :: wp = Kind(0.0d0)
!     .. Scalar Arguments ..
      Real (Kind=wp)                   :: xs, ys
      Integer                          :: ideg, len, n, nleft, nright
      Logical                          :: ok, userw
!     .. Array Arguments ..
      Real (Kind=wp)                   :: rw(n), w(n), y(n)
!     .. Local Scalars ..
      Real (Kind=wp)                   :: a, b, c, h, h1, h9, r, range
      Integer                          :: j
!     .. Intrinsic Procedures ..
      Intrinsic                        :: abs, max, real, sqrt
!     .. Executable Statements ..

      range = real(n,kind=wp) - real(1,kind=wp)
      h = max(xs-real(nleft,kind=wp),real(nright,kind=wp)-xs)
      If (len>n) h = h + real((len-n)/2,kind=wp)
      h9 = 0.999E0_wp*h
      h1 = 0.001E0_wp*h
      a = 0.E0_wp
      Do j = nleft, nright
        r = abs(real(j,kind=wp)-xs)
        If (r<=h9) Then
          If (r<=h1) Then
            w(j) = 1.E0_wp
          Else
            w(j) = (1.E0_wp-(r/h)**3)**3
          End If
          If (userw) w(j) = rw(j)*w(j)
          a = a + w(j)
        Else
          w(j) = 0.E0_wp
        End If
      End Do

      If (a<=0.E0_wp) Then
        ok = .False.
      Else
        ok = .True.
        Do j = nleft, nright
          w(j) = w(j)/a
        End Do
        If ((h>0.E0_wp) .And. (ideg>0)) Then
          a = 0.E0_wp
          Do j = nleft, nright
            a = a + w(j)*real(j,kind=wp)
          End Do
          b = xs - a
          c = 0.E0_wp
          Do j = nleft, nright
            c = c + w(j)*(real(j,kind=wp)-a)**2
          End Do
          If (sqrt(c)>0.001E0_wp*range) Then
            b = b/c
            Do j = nleft, nright
              w(j) = w(j)*(b*(real(j,kind=wp)-a)+1.0E0_wp)
            End Do
          End If
        End If
        ys = 0.E0_wp
        Do j = nleft, nright
          ys = ys + w(j)*y(j)
        End Do
      End If

      Return
    End Subroutine stlest

    Subroutine stlfts(x,n,np,trend,work)

!     .. Implicit None Statement ..
      Implicit None
!     .. Precision Parameter ..
      Integer, Parameter               :: wp = Kind(0.0d0)
!     .. Scalar Arguments ..
      Integer                          :: n, np
!     .. Array Arguments ..
      Real (Kind=wp)                   :: trend(n), work(n), x(n)
!     .. External Procedures ..
      External                         :: stlma
!     .. Executable Statements ..

      Call stlma(x,n,np,trend)
      Call stlma(trend,n-np+1,np,work)
      Call stlma(work,n-2*np+2,3,trend)
      Return
    End Subroutine stlfts


    Subroutine stlma(x,n,len,ave)

!     Moving Average (aka "running mean")
!     ave(i) := mean(x{j}, j = max(1,i-k),..., min(n, i+k))
!           for i = 1,2,..,n

!     implicit none
!     Arg
!     Var

!     .. Implicit None Statement ..
      Implicit None
!     .. Precision Parameter ..
      Integer, Parameter               :: wp = Kind(0.0d0)
!     .. Scalar Arguments ..
      Integer                          :: len, n
!     .. Array Arguments ..
      Real (Kind=wp)                   :: ave(n), x(n)
!     .. Local Scalars ..
      Real (Kind=wp)                   :: flen, v
      Integer                          :: i, j, k, m, newn
!     .. Intrinsic Procedures ..
      Intrinsic                        :: real
!     .. Executable Statements ..
      newn = n - len + 1
      flen = real(len,kind=wp)
      v = 0.E0_wp
      Do i = 1, len
        v = v + x(i)
      End Do
      ave(1) = v/flen
      If (newn>1) Then
        k = len
        m = 0
        Do j = 2, newn
          k = k + 1
          m = m + 1
          v = v - x(m) + x(k)
          ave(j) = v/flen
        End Do
      End If
      Return
    End Subroutine stlma


    Subroutine stlstp(y,n,np,ns,nt,nl,isdeg,itdeg,ildeg,nsjump,ntjump,nljump,  &
      ni,userw,rw,season,trend,work)

!     implicit none
!     Arg
!     Var

!     .. Implicit None Statement ..
      Implicit None
!     .. Precision Parameter ..
      Integer, Parameter               :: wp = Kind(0.0d0)
!     .. Scalar Arguments ..
      Integer                          :: ildeg, isdeg, itdeg, n, ni, nl,      &
                                          nljump, np, ns, nsjump, nt, ntjump
      Logical                          :: userw
!     .. Array Arguments ..
      Real (Kind=wp)                   :: rw(n), season(n), trend(n),          &
                                          work(n+2*np,5), y(n)
!     .. Local Scalars ..
      Integer                          :: i, j
!     .. External Procedures ..
      External                         :: stless, stlfts, stlss
!     .. Executable Statements ..

      Do j = 1, ni
        Do i = 1, n
          work(i,1) = y(i) - trend(i)
        End Do
        Call stlss(work(1,1),n,np,ns,isdeg,nsjump,userw,rw,work(1,2),          &
          work(1,3),work(1,4),work(1,5),season)
        Call stlfts(work(1,2),n+2*np,np,work(1,3),work(1,1))
        Call stless(work(1,3),n,nl,ildeg,nljump,.False.,work(1,4),work(1,1),   &
          work(1,5))
        Do i = 1, n
          season(i) = work(np+i,2) - work(i,1)
        End Do
        Do i = 1, n
          work(i,1) = y(i) - season(i)
        End Do
        Call stless(work(1,1),n,nt,itdeg,ntjump,userw,rw,trend,work(1,3))
      End Do
      Return
    End Subroutine stlstp

    Subroutine stlrwt(y,n,fit,rw)
!     Robustness Weights
!       rw_i := B( |y_i - fit_i| / (6 M) ),   i = 1,2,...,n
!               where B(u) = (1 - u^2)^2  * 1[|u| < 1]   {Tukey's biweight}
!               and   M := median{ |y_i - fit_i| }
!     implicit none
!     Arg
!     Var

!     .. Implicit None Statement ..
      Implicit None
!     .. Precision Parameter ..
      Integer, Parameter               :: wp = Kind(0.0d0)
!     .. Scalar Arguments ..
      Integer                          :: n
!     .. Array Arguments ..
      Real (Kind=wp)                   :: fit(n), rw(n), y(n)
!     .. Local Scalars ..
      Real (Kind=wp)                   :: c1, c9, cmad, r
      Integer                          :: i
!     .. Local Arrays ..
      Integer                          :: mid(2)
!     .. External Procedures ..
      External                         :: psort
!     .. Intrinsic Procedures ..
      Intrinsic                        :: abs
!     .. Executable Statements ..

      Do i = 1, n
        rw(i) = abs(y(i)-fit(i))
      End Do
      mid(1) = n/2 + 1
      mid(2) = n - mid(1) + 1
      Call psort(rw,n,mid,2)
      cmad = 3.0E0_wp*(rw(mid(1))+rw(mid(2)))
!     = 6 * MAD
      c9 = 0.999E0_wp*cmad
      c1 = 0.001E0_wp*cmad
      Do i = 1, n
        r = abs(y(i)-fit(i))
        If (r<=c1) Then
          rw(i) = 1.E0_wp
        Else If (r<=c9) Then
          rw(i) = (1.E0_wp-(r/cmad)**2)**2
        Else
          rw(i) = 0.E0_wp
        End If
      End Do
      Return
    End Subroutine stlrwt

    Subroutine stlss(y,n,np,ns,isdeg,nsjump,userw,rw,season,work1,work2,work3, &
      work4)
!
!       called by stlstp() at the beginning of each (inner) iteration
!
!     implicit none
!     Arg
!     Var

!     .. Implicit None Statement ..
      Implicit None
!     .. Precision Parameter ..
      Integer, Parameter               :: wp = Kind(0.0d0)
!     .. Scalar Arguments ..
      Integer                          :: isdeg, n, np, ns, nsjump
      Logical                          :: userw
!     .. Array Arguments ..
      Real (Kind=wp)                   :: rw(n), season(n+2*np), work1(n),     &
                                          work2(n), work3(n), work4(n), y(n)
!     .. Local Scalars ..
      Real (Kind=wp)                   :: xs
      Integer                          :: i, j, k, m, nleft, nright
      Logical                          :: ok
!     .. External Procedures ..
      External                         :: stless, stlest
!     .. Intrinsic Procedures ..
      Intrinsic                        :: max, min
!     .. Executable Statements ..

      If (np<1) Return

      Do j = 1, np
        k = (n-j)/np + 1
        Do i = 1, k
          work1(i) = y((i-1)*np+j)
        End Do
        If (userw) Then
          Do i = 1, k
            work3(i) = rw((i-1)*np+j)
          End Do
        End If
        Call stless(work1,k,ns,isdeg,nsjump,userw,work3,work2(2),work4)
        xs = 0
        nright = min(ns,k)
        Call stlest(work1,k,ns,isdeg,xs,work2(1),1,nright,work4,userw,work3,   &
          ok)
        If (.Not. ok) work2(1) = work2(2)
        xs = k + 1
        nleft = max(1,k-ns+1)
        Call stlest(work1,k,ns,isdeg,xs,work2(k+2),nleft,k,work4,userw,work3,  &
          ok)
        If (.Not. ok) work2(k+2) = work2(k+1)
        Do m = 1, k + 2
          season((m-1)*np+j) = work2(m)
        End Do

      End Do

      Return
    End Subroutine stlss


!   STL E_Z_ : "Easy" user interface  -- not called from R

    Subroutine stlez(y,n,np,ns,isdeg,itdeg,robust,no,rw,season,trend,work)

!     implicit none
!     Arg
!     Var

!     .. Implicit None Statement ..
      Implicit None
!     .. Precision Parameter ..
      Integer, Parameter               :: wp = Kind(0.0d0)
!     .. Scalar Arguments ..
      Integer                          :: isdeg, itdeg, n, no, np, ns
      Logical                          :: robust
!     .. Array Arguments ..
      Real (Kind=wp)                   :: rw(n), season(n), trend(n),          &
                                          work(n+2*np,7), y(n)
!     .. Local Scalars ..
      Real (Kind=wp)                   :: difs, dift, maxds, maxdt, maxs,      &
                                          maxt, mins, mint
      Integer                          :: i, ildeg, j, newnp, newns, ni, nl,   &
                                          nljump, nsjump, nt, ntjump
!     .. External Procedures ..
      External                         :: stlrwt, stlstp
!     .. Intrinsic Procedures ..
      Intrinsic                        :: abs, int, max, mod, real
!     .. Executable Statements ..

      ildeg = itdeg
      newns = max(3,ns)
      If (mod(newns,2)==0) newns = newns + 1
      newnp = max(2,np)
      nt = int((1.5E0_wp*newnp)/(1.E0_wp-1.5E0_wp/newns)+0.5E0_wp)
      nt = max(3,nt)
      If (mod(nt,2)==0) nt = nt + 1
      nl = newnp
      If (mod(nl,2)==0) nl = nl + 1

      If (robust) Then
        ni = 1
      Else
        ni = 2
      End If

      nsjump = max(1,int(real(newns,kind=wp)/10+0.9_wp))
      ntjump = max(1,int(real(nt,kind=wp)/10+0.9_wp))
      nljump = max(1,int(real(nl,kind=wp)/10+0.9_wp))
      Do i = 1, n
        trend(i) = 0.E0_wp
      End Do
      Call stlstp(y,n,newnp,newns,nt,nl,isdeg,itdeg,ildeg,nsjump,ntjump,       &
        nljump,ni,.False.,rw,season,trend,work)

      no = 0
      If (robust) Then
        j = 1
!        Loop  --- 15 robustness iterations
100     If (j<=15) Then
          Do i = 1, n
            work(i,6) = season(i)
            work(i,7) = trend(i)
            work(i,1) = trend(i) + season(i)
          End Do
          Call stlrwt(y,n,work(1,1),rw)
          Call stlstp(y,n,newnp,newns,nt,nl,isdeg,itdeg,ildeg,nsjump,ntjump,   &
            nljump,ni,.True.,rw,season,trend,work)
          no = no + 1
          maxs = work(1,6)
          mins = work(1,6)
          maxt = work(1,7)
          mint = work(1,7)
          maxds = abs(work(1,6)-season(1))
          maxdt = abs(work(1,7)-trend(1))
          Do i = 2, n
            If (maxs<work(i,6)) maxs = work(i,6)
            If (maxt<work(i,7)) maxt = work(i,7)
            If (mins>work(i,6)) mins = work(i,6)
            If (mint>work(i,7)) mint = work(i,7)
            difs = abs(work(i,6)-season(i))
            dift = abs(work(i,7)-trend(i))
            If (maxds<difs) maxds = difs
            If (maxdt<dift) maxdt = dift
          End Do
          If ((maxds/(maxs-mins)<0.01E0_wp) .And. (maxdt/(maxt-                &
            mint)<0.01E0_wp)) Go To 110
          Continue
          j = j + 1
          Go To 100
        End If
!        end Loop
110     Continue

      Else
!       .not. robust

        Do i = 1, n
          rw(i) = 1.0E0_wp
        End Do
      End If

      Return
    End Subroutine stlez

    Subroutine psort(a,n,ind,ni)
!
!     Partial Sorting ; used for Median (MAD) computation only
!
!     implicit none
!     Arg
!     Var

!     .. Implicit None Statement ..
      Implicit None
!     .. Precision Parameter ..
      Integer, Parameter               :: wp = Kind(0.0d0)
!     .. Scalar Arguments ..
      Integer                          :: n, ni
!     .. Array Arguments ..
      Real (Kind=wp)                   :: a(n)
      Integer                          :: ind(ni)
!     .. Local Scalars ..
      Real (Kind=wp)                   :: t, tt
      Integer                          :: i, ij, j, jl, ju, k, l, m, p
!     .. Local Arrays ..
      Integer                          :: il(16), indl(16), indu(16), iu(16)
!     .. Executable Statements ..

      If (n<0 .Or. ni<0) Return

      If (n<2 .Or. ni==0) Return

      jl = 1
      ju = ni
      indl(1) = 1
      indu(1) = ni
      i = 1
      j = n
      m = 1

!     Outer Loop
100   Continue
      If (i<j) Go To 130

!     _Loop_
110   Continue
      m = m - 1
      If (m==0) Return
      i = il(m)
      j = iu(m)
      jl = indl(m)
      ju = indu(m)
      If (.Not. (jl<=ju)) Go To 110

!     while (j - i > 10)
120   If (.Not. (j-i>10)) Go To 190

130   k = i
      ij = (i+j)/2
      t = a(ij)
      If (a(i)>t) Then
        a(ij) = a(i)
        a(i) = t
        t = a(ij)
      End If
      l = j
      If (a(j)<t) Then
        a(ij) = a(j)
        a(j) = t
        t = a(ij)
        If (a(i)>t) Then
          a(ij) = a(i)
          a(i) = t
          t = a(ij)
        End If
      End If

140   Continue
      l = l - 1
      If (a(l)<=t) Then
        tt = a(l)
150     Continue
        k = k + 1
        If (.Not. (a(k)>=t)) Go To 150

        If (k>l) Go To 160

        a(l) = a(k)
        a(k) = tt
      End If
      Go To 140

160   Continue
      indl(m) = jl
      indu(m) = ju
      p = m
      m = m + 1
      If (l-i<=j-k) Then
        il(p) = k
        iu(p) = j
        j = l

170     Continue
        If (jl>ju) Go To 110
        If (ind(ju)>j) Then
          ju = ju - 1
          Go To 170
        End If
        indl(p) = ju + 1
      Else
        il(p) = i
        iu(p) = l
        i = k

180     Continue
        If (jl>ju) Go To 110
        If (ind(jl)<i) Then
          jl = jl + 1
          Go To 180
        End If
        indu(p) = jl - 1
      End If

      Go To 120
!     end while
190   Continue

      If (i/=1) Then
        i = i - 1
200     Continue
        i = i + 1
        If (i==j) Go To 110
        t = a(i+1)
        If (a(i)>t) Then
          k = i
!           repeat
210       Continue
          a(k+1) = a(k)
          k = k - 1
          If (.Not. (t>=a(k))) Go To 210
!           until  t >= a(k)
          a(k+1) = t
        End If
        Go To 200

      End If

      Go To 100
!     End Outer Loop

    End Subroutine psort
