---
title: "Using Fortran in R Markdown"
author: "John C Nash
    Telfer School of Management,
    University of Ottawa,
    nashjc@uottawa.ca"
date: "July 17, 2018"
output: 
    pdf_document
bibliography: nlm-hessian.bib
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

## APPENDIX A: Using Fortran in Rmarkdown

Notes:

- you must call a subroutine
- R must call .Fortran with all returned variables appropriately filled in
with dummy values.


```{fortran}


C   MAIN
      SUBROUTINE RUNNER(NN, FVAL, X)
      DOUBLE PRECISION X(10), XB(10), W(100), FVAL
      INTEGER INFO, NN
      EXTERNAL FCN
      NN = 2
      X(1) = -1.2
      X(2) = 1
      CALL FCN(2, X, FVAL)
      PRINT 101, FVAL
 101  FORMAT("0 Function value =", 1pe16.8)
 100  FORMAT("1H0", 1P5E16.8)
C     STOP
      
      RETURN
      END
      SUBROUTINE FCN(NN, X, F)
      DOUBLE PRECISION X(NN), F
C     ROSENBROCK
      F = 100*(X(2)-X(1)**2)**2 + (1.0 - X(1))**2
      RETURN
      END
```

Try it! 

```{r}
res = .Fortran("runner", nn=2L, fval=-9999., x=c(-99., -99.))
str(res)
# ./a.out
```


